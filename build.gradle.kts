plugins {
    `java-library`
    idea
    `maven-publish`
    kotlin("jvm")
}

val ver = "0.4"
group = "io.highcreeksoftware"
version = ver

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("junit", "junit", "4.12")

    val handlebarsVersion = "4.2.0"
    api("com.github.jknack:handlebars:$handlebarsVersion")
    implementation("com.github.jknack:handlebars-jackson2:$handlebarsVersion")
    implementation("com.github.jknack:handlebars-markdown:$handlebarsVersion")
    implementation("io.javalin:javalin:3.10.1")
}

// Publish to the local repo -> gradle clean build publishToMavenLocal
publishing {
    publications {

        register("maven", MavenPublication::class) {
            groupId = "io.highcreeksoftware"
            artifactId = "render"
            version = ver
            artifact("build/libs/render-$ver.jar")

            pom.withXml {
                val dependenciesNode = asNode().appendNode("dependencies")
                val configNames = arrayOf("implementation", "api")
                configNames.forEach { configName ->
                    configurations[configName].allDependencies.forEach {
                        if(it.group != null) {
                            val depNode = dependenciesNode.appendNode("dependency")
                            depNode.appendNode("groupId", it.group)
                            depNode.appendNode("artifactId", it.name)
                            depNode.appendNode("version", it.version)
                        }
                    }
                }
            }
        }
    }
}