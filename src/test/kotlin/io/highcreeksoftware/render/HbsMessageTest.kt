package io.highcreeksoftware.render

import io.highcreeksoftware.render.helpers.HostHelper
import org.junit.Assert
import org.junit.Test

class HbsMessageTest {

    @Test
    fun testBaseMethod() {
        val message = HbsMessage(false)
        message.registerHelper("base", HostHelper("http://localhost:3000"))

        val result = message.render("testbase", mutableMapOf<String, Any>("test" to "data"))

        Assert.assertEquals("http://localhost:3000", result)
    }
}