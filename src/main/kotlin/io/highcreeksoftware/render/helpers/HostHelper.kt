package io.highcreeksoftware.render.helpers

import com.github.jknack.handlebars.Handlebars
import com.github.jknack.handlebars.Helper
import com.github.jknack.handlebars.Options
import java.util.*

class HostHelper(val host: String): Helper<LinkedHashMap<Any, Any>> {
    override fun apply(context: LinkedHashMap<Any, Any>?, options: Options?): Any {
        return Handlebars.SafeString(host)
    }
}