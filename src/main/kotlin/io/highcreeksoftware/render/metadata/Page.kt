package io.highcreeksoftware.render.metadata

class Page {
    var key: String? = null
    var title: String? = null
    var description: String? = null
    var canonicalUrl: String? = null
    var css: Array<String>? = null
    var js: Array<String>? = null
}