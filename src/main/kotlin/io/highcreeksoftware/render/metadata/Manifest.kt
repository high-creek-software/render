package io.highcreeksoftware.render.metadata

class Manifest {
    val pages: HashMap<String, Page>? = null
    var css: HashMap<String, Asset>? = null
    var js: HashMap<String, Asset>? = null
}