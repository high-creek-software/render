package io.highcreeksoftware.render.flashes

import io.javalin.http.Context
import java.util.*

class CookieFlashManager: FlashManager {

    private val KEY_ERROR = "error"
    private val KEY_SUCCESS = "success"

    override fun setErrorFlash(error: String, context: Context) {
        setFlash(KEY_ERROR, error, context)
    }

    override fun getErrorFlash(context: Context): String? {
        val err = context.cookie(KEY_ERROR) ?: return null
        context.removeCookie(KEY_ERROR, "/")
        return String(Base64.getDecoder().decode(err))
    }

    override fun setSuccessFlash(success: String, context: Context) {
        setFlash(KEY_SUCCESS, success, context)
    }

    override fun getSuccessFlash(context: Context): String? {
        val success = context.cookie(KEY_SUCCESS) ?: return null
        context.removeCookie(KEY_SUCCESS, "/")
        return String(Base64.getDecoder().decode(success))
    }

    private fun setFlash(key: String, message: String, context: Context) {
        context.cookie(key, Base64.getEncoder().encodeToString(message.toByteArray()))
    }
}