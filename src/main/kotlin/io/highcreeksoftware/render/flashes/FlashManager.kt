package io.highcreeksoftware.render.flashes

import io.javalin.http.Context

interface FlashManager {
    fun setErrorFlash(error: String, context: Context)
    fun getErrorFlash(context: Context): String?
    fun setSuccessFlash(success: String, context: Context)
    fun getSuccessFlash(context: Context): String?
}