package io.highcreeksoftware.render

import com.github.jknack.handlebars.Handlebars
import com.github.jknack.handlebars.Helper
import com.github.jknack.handlebars.Jackson2Helper
import com.github.jknack.handlebars.MarkdownHelper
import com.github.jknack.handlebars.helper.ConditionalHelpers
import com.github.jknack.handlebars.helper.StringHelpers
import com.github.jknack.handlebars.io.ClassPathTemplateLoader
import com.github.jknack.handlebars.io.FileTemplateLoader
import io.highcreeksoftware.render.flashes.FlashManager
import io.highcreeksoftware.render.metadata.Manifest
import io.javalin.http.Context
import io.javalin.plugin.rendering.FileRenderer
import kotlin.collections.HashMap

open class HbsRenderer(val flashManager: FlashManager? = null, val prod: Boolean = false, val tplsPrefix: String = "tpls", val manifest: Manifest? = null): FileRenderer {

    private val handlebars: Handlebars

    init {
        if(prod) {
            handlebars = Handlebars(ClassPathTemplateLoader().apply {
                prefix = "/$tplsPrefix"
            })
        } else {
            handlebars = Handlebars(FileTemplateLoader("./src/main/resources/$tplsPrefix/"))
        }

        handlebars.registerHelpers(ConditionalHelpers::class.java)
        handlebars.registerHelpers(StringHelpers::class.java)
        handlebars.registerHelper("md", MarkdownHelper())
        handlebars.registerHelper("json", Jackson2Helper.INSTANCE)
    }

    override fun render(filePath: String, model: MutableMap<String, Any>, context: Context): String {
        val merged: HashMap<String, Any> = HashMap()
        for((k,v) in model) {
            merged[k] = v
        }

        val tplName = sanitizeName(filePath)
        val tpl = handlebars.compile(tplName)
        // Check for error/success flashes
        val error = flashManager?.getErrorFlash(context)
        if(error != null) {
            merged["error"] = error
        }
        val success = flashManager?.getSuccessFlash(context)
        if(success != null) {
            merged["success"] = success
        }
        merged["prod"] = prod
        merged["tpl"] = tplName
        val page = manifest?.pages?.get(filePath)
        if(page != null) {
            model["page"] = page
        }

        dataHook(merged, context)
        return tpl.apply(merged)
    }

    fun <H> registerHelper(name: String, helper: Helper<H>) {
        handlebars.registerHelper(name, helper)
    }

    fun registerHelpers(helperSource: Any) {
        handlebars.registerHelpers(helperSource)
    }

    open fun dataHook(model: HashMap<String, Any>, context: Context) {

    }

    private fun sanitizeName(name: String): String {
        var wip = name
        if(name.startsWith("/")) {
            wip = name.replaceFirst("/", "")
        }
        wip = wip.replace(".hbs", "")
        println("Input: $name Sanitized: $wip")
        return wip
    }

    fun safeString(input: String): Handlebars.SafeString = Handlebars.SafeString(input)

    companion object {
        const val EXT = ".hbs"
    }
}