package io.highcreeksoftware.render

import com.github.jknack.handlebars.Handlebars
import com.github.jknack.handlebars.Helper
import com.github.jknack.handlebars.Jackson2Helper
import com.github.jknack.handlebars.MarkdownHelper
import com.github.jknack.handlebars.helper.ConditionalHelpers
import com.github.jknack.handlebars.helper.StringHelpers
import com.github.jknack.handlebars.io.ClassPathTemplateLoader
import com.github.jknack.handlebars.io.FileTemplateLoader

class HbsMessage(val prod: Boolean = false, val tplsPrefix: String = "msgs") {

    private val handlebars: Handlebars

    init {
        if(prod) {
            handlebars = Handlebars(ClassPathTemplateLoader().apply {
                prefix = "/$tplsPrefix"
            })
        } else {
            handlebars = Handlebars(FileTemplateLoader("./src/main/resources/$tplsPrefix/"))
        }

        handlebars.registerHelpers(ConditionalHelpers::class.java)
        handlebars.registerHelpers(StringHelpers::class.java)
        handlebars.registerHelper("md", MarkdownHelper())
        handlebars.registerHelper("json", Jackson2Helper.INSTANCE)
    }

    fun <H> registerHelper(name: String, helper: Helper<H>) {
        handlebars.registerHelper(name, helper)
    }

    fun registerHelpers(helperSource: Any) {
        handlebars.registerHelpers(helperSource)
    }

    fun render(name: String, model: MutableMap<String, Any>? = null): String {
        val tpl = handlebars.compile(name)
        return tpl.apply(model)
    }
}